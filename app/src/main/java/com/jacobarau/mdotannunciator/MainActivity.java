package com.jacobarau.mdotannunciator;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import 	android.os.AsyncTask;
import 	android.speech.tts.TextToSpeech;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;


public class MainActivity extends ActionBarActivity {

    ArrayList<Incident> incidents;
    TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                new GetIncidentListTask().execute(0);
            }
        }
        );




    }

    // At this point we don't use the Integer for params; I don't know how now to take params.
    private class GetIncidentListTask extends AsyncTask<Integer, Integer, JSONObject> {
        String text = null;
        protected JSONObject doInBackground(Integer... params) {
            /*for (int i = 0; i < count; i++) {
                totalSize += Downloader.downloadFile(urls[i]);
                publishProgress((int) ((i / (float) count) * 100));
                // Escape early if cancel() is called
                if (isCancelled()) break;
            }*/
            BufferedReader in;
            JSONObject jObject = null;

            try{
                HttpClient httpclient = new DefaultHttpClient();

                HttpGet request = new HttpGet();
                URI website = new URI("http://mdotnetpublic.state.mi.us/drive/DataServices.asmx/fetchIncidents");
                request.setURI(website);
                HttpResponse response = httpclient.execute(request);
                in = new BufferedReader(new InputStreamReader(
                        response.getEntity().getContent()));

                StringBuilder builder = new StringBuilder();
                String aux;

                while ((aux = in.readLine()) != null) {
                    builder.append(aux);
                }

                text = builder.toString();
                Log.d("log_tag",text);

                jObject = new JSONObject(text);


            }catch(Exception e){
                Log.e("log_tag", "Error in http connection " + e.toString());
            }
            return jObject;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(JSONObject result) {
            if (result == null) {
                Log.d("log_tag","unable to parse data");
                tts.speak(("Unable to get incident data."), TextToSpeech.QUEUE_ADD, null);
                return;
            }
            try {
                JSONArray d = result.getJSONArray("d");
                int dlen = d.length();
                if (dlen > 0) {
                    //tts.speak((CharSequence) (dlen + " Active incidents present."), TextToSpeech.QUEUE_ADD, null, null);
                    tts.speak((dlen + " Active incidents present."), TextToSpeech.QUEUE_ADD, null);
                } else {
                    tts.speak(("No active incidents present."), TextToSpeech.QUEUE_ADD, null);
                }
                incidents =  new ArrayList<>();
                for (int i = 0; i < dlen; i++) {
                    Incident newIncident = new Incident();
                    JSONObject j = d.getJSONObject(i);
                    newIncident.id = j.getInt("id");
                    newIncident.latitude = j.getDouble("latitude");
                    newIncident.longitude = j.getDouble("longitude");
                    newIncident.message = android.text.Html.fromHtml(j.getString("message")).toString();
                    newIncident.title = j.getString("title");
                    incidents.add(newIncident);
                    Log.d("log_tag",newIncident.message);

                    String textToSpeak = "";
                    Scanner in = new Scanner(newIncident.message);
                    in.useDelimiter("\n");
                    String route = "";
                    String lanes = "";
                    while (in.hasNext()) {
                        String s = in.next();
                        Log.d("log_tag", "line is " + s);
                        if (s.contains("Route:")) {
                            s = s.replace("Route:", "");
                            route = s;
                            Log.d("log_tag", "Found route; it is " + route);
                        }
                        if (s.contains("Lanes Affected:")) {
                            s = s.replace("Lanes Affected:", "");
                            s = s.replace(",", " and ");
                            s = s + " affected";
                            lanes = s;
                            Log.d("log_tag", "Found lane; it is " + lanes);
                        }
                    }

                    tts.speak("Incident " + (i+1) + ". " + newIncident.title + ", " + route + ", " + lanes, TextToSpeech.QUEUE_ADD, null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    class Incident {
        public Double latitude;
        public Double longitude;
        public int id;
        public String message;
        public String title;
    }
}
